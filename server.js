const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const compression = require('compression');
const mongoose = require('mongoose');
const app = express();


const config = require('./config/config.json')
const mongoConnectionString = config.MONGODB
const port = process.env.PORT || config.PORT
const secret = config.SECRET

app.set("secretKey", secret)

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(helmet())
app.use(compression())

mongoose.connect(mongoConnectionString, { useUnifiedTopology: true, useNewUrlParser: true, authSource: 'admin', dbName: 'test', autoIndex: false, promiseLibrary: 'global.Promise' }, (err, connect) => {
        if (err) console.log('=-=-=-=mongoDb err-=-=-=>', err)
        else console.log('Connected to Database')
    })
    // mongoose.set('useCreateIndex', true);


const routes = require('./routes/routes')
app.use('/', routes)

app.listen(port, () => {
    console.log(`Listening on port ${port}`)
})