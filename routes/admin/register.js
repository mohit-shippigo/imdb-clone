const dbAdminLogin = require('../../model/adminLogin');

module.exports = async(req, res) => {
    try {

        let adminData = await dbAdminLogin.findOne({ $or: [{ email: req.body.email }, { phone: req.body.phone }] })

        if (!adminData || adminData == null) {
            let savedData = await new dbAdminLogin({
                email: req.body.email,
                password: req.body.password,
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                user: "admin"
            }).save()

            if (!savedData) {
                res.json({
                    success: false,
                    msg: "Error while registering Admin. Please try after some time"
                })
            } else {
                res.json({
                    success: true,
                    msg: "Registeration successful."
                })
            }
        } else {
            res.json({
                success: false,
                msg: "You have already registered."
            })
        }


    } catch (err) {
        console.log(`Exception caught in ${__filename} with error :- ${err}`)
        res.json({
            success: false,
            msg: err
        })
    }

}