var dbAdminLogin = require('../../model/adminLogin');
var jwt = require('jsonwebtoken')


exports.login = async(req, res) => {
    try {

        let loginData = await dbAdminLogin.findOne({ email: req.body.email })

        if (!loginData || loginData == null) {
            res.json({
                success: false,
                msg: "No admin found"
            })
        } else {
            let isMatch = await loginData.comparePassword(req.body.password)

            if (!isMatch) {
                res.json({
                    success: false,
                    msg: "Incorrect Password."
                })
            } else {
                if (isMatch || loginData.password === req.body.password) {

                    let data = {
                        email: loginData.email,
                        name: loginData.firstName + loginData.lastName,
                        user: 'admin'
                    }
                    let token = jwt.sign(data, req.app.get('secretKey'))

                    let update = await dbAdminLogin.findOneAndUpdate({ email: req.body.email }, { $set: { token: token } })

                    if (!update) {
                        res.json({
                            success: false,
                            msg: "Something went wrong."
                        })
                    } else {
                        res.json({
                            success: true,
                            msg: "Login Succesfull",
                            token: token
                        })
                    }


                }
            }

        }


    } catch (err) {
        console.log(`Exception caught in login ${__filename} with error :- ${err}`)
        res.json({
            success: false,
            msg: err
        })
    }

}

exports.logout = async(req, res) => {
    try {

        let logout = await dbAdminLogin.findOneAndUpdate({ email: req.body.email }, { $set: { token: null } })

        if (!logout) {
            res.json({
                success: false,
                msg: "No such email id found in database."
            })
        } else {
            res.json({
                success: true,
                msg: "Logout succcessfully"
            })
        }

    } catch (err) {
        console.log(`Exception caught in logout ${__filename} with error :- ${err}`)
        res.json({
            success: false,
            msg: err
        })
    }
}