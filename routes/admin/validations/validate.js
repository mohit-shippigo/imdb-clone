const register = require('./register');
const login = require('./login');
const logout = require('./logout');

module.exports = (req, res, next) => {
    const url = req.originalUrl.split('/');
    const finalUrl = url[url.length - 1]
    switch (finalUrl) {

        /**
         * Register
         */
        case 'register':
            register(req, res, next)
            break;

            /**
             * Login
             */
        case 'login':
            login(req, res, next)
            break;

            /**
             * Logout
             */
        case 'logout':
            logout(req, res, next)
            break;


        default:
            next();
    }

}