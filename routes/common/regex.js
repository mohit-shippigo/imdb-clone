module.exports = {
    emailRegex: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    regexWordsWithSpace: new RegExp('^[a-zA-Z][a-zA-Z ]*$'),
    regexNum: new RegExp('^([1-9][0-9]*)$'),
    posNumberRegex: /^\d*[0-9]\d*$/,
    regexAlphaNum: new RegExp('^\s*([0-9a-zA-Z]*)\s*$'),
    codRegex: /^\s*(?=.*[0-9])\d*(?:\.\d{0,2})?\s*$/,
    integerRegex: /^\s*(?=.*[1-9])\d*(?:\.\d{0,2})?\s*$/,
    regexAlphabets: new RegExp('^[a-zA-Z]*$'),
    phoneRegex: /^(?:(?:\+|0{0,2})91(\s*[\ -]\s*)?|[0]?)?[6-9]\d{9}|(\d[ -]?){10}\d$/,
    passwordRegex: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
    pincodeRegex: new RegExp("^[1-9][0-9]{5}$"),
    noSpecialCharacterRegex: new RegExp('^[0-9a-zA-Z][0-9a-zA-Z ]*$'),
    panRegex: /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/,
    ifscRegex: /^[A-Za-z]{4}\d{7}$/,
    regexacno: /^([0-9]*)$/,
    replaceEmoji: /[^\x00-\x7F]/g,
    dateRegex: /^(0?[1-9]|[1-2][0-9]|3[0-1])-(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)-[1-2][0-9]{3}$/
}