const dbActor = require('../../model/actors');

module.exports = async(req, res) => {
    try {

        let actors = await dbActor.find({})

        if (!actors) {
            res.json({
                success: false,
                msg: "No Actor/Actress found in database"
            })
        } else {
            let actorArr = []
            actors.forEach(ele => {
                let actor = ele.firstName + ' ' + ele.lastName
                actorArr.push(actor)

            })

            res.json({
                success: true,
                msg: actorArr
            })
        }

    } catch (err) {
        console.log(`Exception caught in ${__filename} with error :- ${err}`)
        res.json({
            success: false,
            msg: err
        })
    }
}