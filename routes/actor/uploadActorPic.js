const multer = require('multer')
const dbActor = require('../../model/actors')

module.exports = (req, res) => {
    try {
        if (req.decoded.user == 'user') {
            res.json({
                success: false,
                msg: "You are not allowed to upload"
            })
        } else {

            /*********************************** Storage Part ***********************************/
            let storage = multer.diskStorage({
                destination(req, file, cb) {
                    cb(null, './routes/actor/uploads/')
                },
                filename(req, file, cb) {
                    cb(null, file.fieldname + '-' + Date.now() + file.originalname)
                }
            })

            /*********************************** Upload Data Configuration ***********************************/
            var upload = multer({
                storage: storage,
                limits: { fileSize: 1024 * 1024 * 5 },
                fileFilter(req, file, cb) {
                    const filetypes = ['image/png', 'image/jpg', 'image/jpeg', 'image/gif']
                    if (filetypes.includes(file.mimetype)) {
                        return cb(null, true)
                    } else {
                        console.log('%%%%%%%%%%format not supported %%%%%%%%', new Error())
                        return cb('Error: Images Only!')
                    }
                }

            }).single('imageName')

            /*********************************** Upload Part ***********************************/
            upload(req, res, (err) => {

                if (err) {
                    console.log('err >>>>> maximum file size 5mb ', err)
                    res.json({
                        success: false,
                        msg: err
                    });
                } else {
                    let firstName = req.body.firstName.toLowerCase().trim()
                    let lastName = req.body.lastName.toLowerCase().trim()

                    let userName = firstName.concat(lastName)

                    if (req.file == undefined || !userName) {
                        res.json({
                            msg: 'Error: No File Selected! or firstname and lastname of actor/actoress is not present.'
                        });
                    } else {
                        dbActor.findOneAndUpdate({ userName: userName }, { $push: { imageName: req.file.filename } }, (err, saved) => {
                            if (err) {
                                console.log('%%%%%%%%%%%%%%%%% error while uploading Actor Image %%%%%%%%%%%%%%', err)
                                res.json({
                                    success: false,
                                    msg: 'Something went wrong'
                                })
                            } else if (!saved) {
                                res.json({
                                    success: false,
                                    msg: 'No such actor found.'
                                })
                            } else {
                                console.log('%%%%%%%%%%%%%%%%% uploaded image and data %%%%%%%%%%%%%%', saved)
                                res.json({
                                    msg: 'File Uploaded!',
                                    file: `uploads/${req.file.filename}`,
                                    pic: saved.filename
                                });
                            }
                        })

                    }
                }
            });
        }


    } catch (err) {
        console.log(`Exception caught in ${__filename} with error :- ${err}`)
        res.json({
            success: false,
            msg: err
        })
    }
}