const express = require('express');
const tokenVerify = require('../common/tokenVerify');
const validate = require('./validations/validate')
const router = express.Router();


/************************** Add Actor Data ************************/
router.post('/addActor', validate, tokenVerify, require('./addActor'))


/************************** Get Actor ************************/
router.get('/getActors', tokenVerify, require('./getActors'))



/************************** Get Actor's Movie ************************/
router.post('/actorMovies', validate, tokenVerify, require('./getAllMoviesofActor'))


/************************** Add Actor Image ************************/
router.post('/addActorImage', tokenVerify, require('./uploadActorPic'))


module.exports = router;