const regex = require('../../common/regex');

module.exports = (req, res, next) => {

    if (!req.body.actorName) {
        res.json({
            success: false,
            msg: "Please fill firstName & lastName of Actor/Actoress."
        })
    } else {
        req.body.actorName = req.body.actorName.toLowerCase().trim()

        let userNameCheck = regex.regexWordsWithSpace.test(req.body.actorName)

        if (!userNameCheck) {
            res.json({
                success: false,
                msg: "Invalid name format."
            })
        } else {
            next();
        }
    }
}