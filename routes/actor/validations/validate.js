const addActor = require('./addActor');
const getAllMoviesofActor = require('./getAllMoviesofActor')
    // const uploadPic = require('./uploadActorPic')

module.exports = (req, res, next) => {
    const url = req.originalUrl.split('/');
    const finalUrl = url[url.length - 1]
    switch (finalUrl) {

        /**
         * Add Actor
         */
        case 'addActor':
            addActor(req, res, next)
            break;

            /**
             * Movies of an Actor
             */
        case 'actorMovies':
            getAllMoviesofActor(req, res, next)
            break;


        default:
            next();
    }

}