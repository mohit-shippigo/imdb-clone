const regex = require('../../common/regex');

module.exports = (req, res, next) => {
    if (!req.body.firstName || !req.body.lastName || !req.body.age) {
        res.json({
            success: false,
            msg: "Please fill name & age of Actor/Actoress."
        })
    } else {
        req.body.firstName = req.body.firstName.toLowerCase().trim()
        req.body.lastName = req.body.lastName.toLowerCase().trim()

        let firstNameCheck = regex.regexAlphabets.test(req.body.firstName)
        let lastNameCheck = regex.regexAlphabets.test(req.body.lastName)
        if (!firstNameCheck || !lastNameCheck) {
            res.json({
                success: false,
                msg: "Invalid name entered."
            })
        } else {
            next();
        }
    }
}