const dbMovie = require('../../model/movies')

module.exports = async(req, res) => {
    try {

        let movies = await dbMovie.aggregate([
            { $match: { cast: req.body.actorName } },
            {
                $group: {
                    _id: { movie: "$movieName" },
                    total: { $sum: 1 }
                }
            },
        ]);


        let moviesArr = [];

        if (movies.length == 0) {
            res.json({
                success: false,
                msg: 'No movie found for this actor.'
            })
        } else {
            movies.forEach(name => {
                moviesArr.push(name._id.movie)
            })

            res.json({
                success: true,
                totalMovies: moviesArr.length,
                msg: moviesArr
            })
        }

    } catch (err) {
        console.log(`Exception caught in ${__filename} with error :- ${err}`)
        res.json({
            success: false,
            msg: err
        })
    }

}