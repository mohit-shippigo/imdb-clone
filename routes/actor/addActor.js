const dbActor = require('../../model/actors');
const dbMovie = require('../../model/movies')

module.exports = async(req, res) => {
    try {
        if (req.decoded.user == 'user') {
            res.json({
                success: false,
                msg: "You are not allowed to add actors."
            })
        } else {
            req.body.userName = req.body.firstName.concat(' ', req.body.lastName)

            /**** Search Actor's Movie ***/
            let actorMoviesData = await dbMovie.find({ cast: req.body.userName })

            let actorMovies = [];
            actorMoviesData.forEach(ele => {
                actorMovies.push(ele.movieName)
            })

            /**** Searching Actor data into database  ****/
            let userName = req.body.firstName + req.body.lastName
            let actorData = await dbActor.findOne({ userName: userName })
            if (actorData) {
                res.json({
                    success: false,
                    msg: "Actor details already stored in database"
                })
            } else {
                /****** If no data present in DB, then saving data ******/
                let savedData = new dbActor({
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    userName: req.body.firstName + req.body.lastName,
                    age: req.body.age,
                    movies: actorMovies
                }).save()

                if (!savedData) {
                    res.json({
                        success: false,
                        msg: "Error while saving Actor data. Please try after some time."
                    })
                } else {
                    res.json({
                        success: true,
                        msg: "Actor data saved successfully."
                    })
                    console.log(__filename, '------ actor data added successfully ------')
                }

            }

        }

    } catch (err) {
        console.log(`Exception caught in ${__filename} with error :- ${err}`)
        res.json({
            success: false,
            msg: "Something went wrong. Please try again later."
        })
    }
}