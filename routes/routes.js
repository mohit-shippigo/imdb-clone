const express = require('express')
const router = express.Router()


/************************** Actor **************************/
router.use('/actor', require('./actor/routes'))



/************************** Admin **************************/
router.use('/admin', require('./admin/routes'))



/************************** Movies **************************/
router.use('/movie', require('./movie/routes'))



/************************** User **************************/
router.use('/user', require('./user/routes'))

module.exports = router