const dbMovie = require('../../model/movies');

module.exports = async(req, res) => {
    try {

        let moviesData = await dbMovie.find({ genre: req.body.genre.toLowerCase() })

        if (!moviesData || moviesData.length === 0 || moviesData === null) {
            res.json({
                success: false,
                msg: "No movie found for such Genre"
            })
        } else {
            let movieGenre = moviesData.map(ele => ele.movieName)
            res.json({
                success: true,
                msg: movieGenre
            })
        }



    } catch (err) {
        console.log(`Exception caught in ${__filename} with error :- ${err}`)
        res.json({
            success: false,
            msg: err
        })
    }
}