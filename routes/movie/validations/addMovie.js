const regex = require('../../common/regex');

module.exports = (req, res, next) => {

    if (!req.body.movieName || !req.body.director || !req.body.cast || !req.body.genre || !req.body.releaseDate || !req.body.description) {
        res.json({
            success: false,
            msg: 'Please fill all details'
        })
    } else {
        req.body.movieName = req.body.movieName.toLowerCase().trim()
        req.body.director = req.body.director.toLowerCase().trim()
        req.body.genre = req.body.genre.toLowerCase().trim()
        req.body.releaseDate = req.body.releaseDate.trim()
        req.body.description = req.body.description.toLowerCase().trim()



        let movieNameCheck = regex.noSpecialCharacterRegex.test(req.body.movieName)
        let directorNameCheck = regex.regexWordsWithSpace.test(req.body.director)
        let genreNameCheck = regex.regexAlphabets.test(req.body.genre)
        let releaseDateCheck = regex.dateRegex.test(req.body.releaseDate)


        if (!movieNameCheck || !directorNameCheck || !genreNameCheck || !releaseDateCheck) {
            res.json({
                success: false,
                msg: "Invalid data format."
            })
        } else {
            next();
        }
    }
}