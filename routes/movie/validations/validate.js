const addMovie = require('./addMovie')
const movieAccGenre = require('./movieAccGenre')

module.exports = (req, res, next) => {
    const url = req.originalUrl.split('/');
    const finalUrl = url[url.length - 1]
    switch (finalUrl) {

        /**
         * Add Movie
         */
        case 'addMovie':
            addMovie(req, res, next)
            break;


            /**
             * Movie Acc. Genre
             */
        case 'movieAccGenre':
            movieAccGenre(req, res, next)
            break;



        default:
            next();
    }

}