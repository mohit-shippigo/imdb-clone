const regex = require('../../common/regex');

module.exports = (req, res, next) => {

    if (!req.body.genre) {
        res.json({
            success: false,
            msg: "Please enter genre."
        })
    } else {
        req.body.genre = req.body.genre.toLowerCase().trim()


        let genreNameCheck = regex.regexAlphabets.test(req.body.genre)


        if (!genreNameCheck) {
            res.json({
                success: false,
                msg: "Invalid genre name."
            })
        } else {
            next();
        }
    }
}