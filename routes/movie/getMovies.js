const dbMovie = require('../../model/movies');

module.exports = async(req, res) => {
    try {

        let movies = await dbMovie.find({})

        if (!movies) {
            res.json({
                success: false,
                msg: "No movie in database"
            })
        } else {
            let movieArr = []
            movies.forEach(ele => {
                let movie = ele.movieName
                movieArr.push(movie)

            })

            res.json({
                success: true,
                msg: movieArr
            })
        }

    } catch (err) {
        console.log(`Exception caught in ${__filename} with error :- ${err}`)
        res.json({
            success: false,
            msg: err
        })
    }
}