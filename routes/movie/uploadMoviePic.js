const multer = require('multer')
const dbMovie = require('../../model/movies')

module.exports = (req, res) => {
    try {
        if (req.decoded.user == 'user') {
            res.json({
                success: false,
                msg: "You are not allowed to upload."
            })
        } else {
            /*********************************** Storage Part ***********************************/
            let storage = multer.diskStorage({
                destination(req, file, cb) {
                    cb(null, './routes/movie/uploads/')
                },
                filename(req, file, cb) {
                    cb(null, file.fieldname + '-' + Date.now() + file.originalname)
                }
            })

            /*********************************** Upload Data Configuration ***********************************/
            var upload = multer({
                storage: storage,
                limits: { fileSize: 1024 * 1024 * 5 },
                fileFilter(req, file, cb) {
                    const filetypes = ['image/png', 'image/jpg', 'image/jpeg', 'image/gif']
                    if (filetypes.includes(file.mimetype)) {
                        return cb(null, true)
                    } else {
                        console.log('%%%%%%%%%%format not supported %%%%%%%%', new Error())
                        return cb('Error: Images Only!')
                    }
                }

            }).single('imageName')

            /*********************************** Upload Part ***********************************/
            upload(req, res, (err) => {
                console.log('=-==-=-=-=-=-=-=-=-=-=-=->', req.file)
                if (err) {
                    console.log('err >>>>> maximum file size 5mb ', err)
                    res.json({
                        success: false,
                        msg: err
                    });
                } else {
                    if (req.file == undefined || !req.body.movieName) {
                        res.json({
                            msg: 'Error: No File Selected! or no Movie Name '
                        });
                    } else {
                        dbMovie.findOneAndUpdate({ movieName: req.body.movieName }, { $push: { imageName: req.file.filename } }, (err, saved) => {
                            if (err) {
                                console.log('%%%%%%%%%%%%%%%%% uploaded image error %%%%%%%%%%%%%%', err)
                                res.json({
                                    success: false,
                                    msg: 'Something went wrong'
                                })
                            } else {
                                console.log('%%%%%%%%%%%%%%%%% uploaded image and data %%%%%%%%%%%%%%', saved)
                                res.json({
                                    msg: 'File Uploaded!',
                                    file: `uploads/${req.file.filename}`
                                });
                            }
                        })

                    }
                }
            });
        }

    } catch (err) {
        console.log(`Exception caught in ${__filename} with error :- ${err}`)
        res.json({
            success: false,
            msg: err
        })
    }
}