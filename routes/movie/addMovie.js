const dbmovie = require('./../../model/movies');
const dbActor = require('../../model/actors');
const regex = require('../common/regex');

module.exports = async(req, res) => {
    try {

        if (req.decoded.user == 'user') {
            res.json({
                success: false,
                msg: "You are not allowed to add movies."
            })
        } else {
            /**
             * Regex for cast Name
             */
            let castNameCheck = [];
            for (let i = 0; i < req.body.cast.length; i++) {
                castNameCheck.push(regex.regexWordsWithSpace.test(req.body.cast[i]))
            }

            if (castNameCheck.includes(false)) {
                res.json({
                    success: false,
                    msg: 'Please check cast input format.'
                })
            } else {
                /*
                 *  cast name to lowerCase
                 */
                let cast = [];
                for (let i = 0; i < req.body.cast.length; i++) {
                    cast.push(req.body.cast[i].toLowerCase().trim())
                }

                let movie = await dbmovie.findOne({ movieName: req.body.movieName })

                if (movie || movie != null) {
                    res.json({
                        success: false,
                        msg: 'This movie already exists in database.'
                    })
                } else {

                    let arr = []

                    for (let key of req.body.cast) {
                        arr.push(key.toLowerCase().replace(/\s/g, ''));
                    }

                    await dbActor.updateMany({ userName: { $in: arr } }, { $push: { movies: req.body.movieName } })

                    let newMovie = await new dbmovie({
                        movieName: req.body.movieName,
                        director: req.body.director,
                        cast: cast,
                        genre: req.body.genre,
                        description: req.body.description,
                        releaseDate: req.body.releaseDate,
                        trailerLink: req.body.trailerLink
                    }).save()

                    if (!newMovie) {
                        res.json({
                            success: false,
                            msg: "Error while saving data. Please try again after some time."
                        })
                    } else {
                        res.json({
                            success: true,
                            msg: 'movie details added successfully.'
                        })
                        console.log(__filename, '------ movie details added successfully ------')
                    }
                }
            }
        }
    } catch (err) {
        console.log(`Exception caught in ${__filename} with error :- ${err.message}`)
        res.json({
            success: false,
            msg: err
        })
    }
}