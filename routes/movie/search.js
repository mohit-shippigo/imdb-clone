const dbMovie = require('../../model/movies');
const dbActor = require('../../model/actors');

module.exports = async(req, res) => {
    try {
        if (!req.body.search) {
            res.json({
                success: false,
                msg: "Please provide actor name or movie name"
            })
        } else {
            let regX = new RegExp(req.body.search, 'i')

            let movieData = await dbMovie.find({ movieName: regX });
            let actorData = await dbActor.find({ userName: regX });

            /**
             *  Actor array
             */
            let actorArr = [];
            actorData.forEach(ele => actorArr.push({ actor: ele.userName, movies: ele.movies }))

            /**
             * Movie Array
             */
            let movieArr = movieData.map(ele => {
                return {
                    movieName: ele.movieName,
                    cast: ele.cast,
                    rating: ele.rating.reduce((prev, current) => prev + current.rate, 0) / ele.rating.length,
                    comments: ele.comments.map(comment => comment.content)
                }
            })


            if (actorArr.length == 0 & movieArr.length == 0) {
                res.json({
                    success: false,
                    msg: "No data found for this query"
                })
            } else {
                res.json({
                    success: true,
                    actors: actorArr,
                    movies: movieArr
                })
            }

        }
    } catch (err) {
        console.log(`Exception caught in ${__filename} with error :- ${err}`)
        res.json({
            success: false,
            msg: "Something went wrong. Please try again later."
        })
    }

}