const dbMovie = require('../../model/movies');

module.exports = async(req, res) => {
    try {
        let todayDate = new Date();
        let threeMonthBackDate = new Date().setDate(todayDate.getDate() - 365);

        let startDate = new Date(threeMonthBackDate);
        let endDate = new Date();

        let moviesData = await dbMovie.find({ releaseDate: { $gt: startDate, $lte: endDate } }).sort({ releaseDate: -1 })

        if (!moviesData || moviesData.length === 0) {
            res.json({
                success: false,
                msg: 'No latest movie found in a year.'
            })
            console.log(__filename, '=-= No Latest Movies found in a year =-=>')
        } else {
            let latestMovie = moviesData.map(ele => ele.movieName)
            res.json({
                success: true,
                msg: latestMovie
            })
            console.log(__filename, '=-= Latest Movies =-=>', latestMovie)
        }

    } catch (err) {
        console.log(`Exception caught in ${__filename} with error :- ${err}`)
        res.json({
            success: false,
            msg: err
        })
    }
}