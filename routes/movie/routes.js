const express = require('express');
const router = express.Router();
const tokenVerify = require('../common/tokenVerify');
const validate = require('./validations/validate');


// Add movie
router.post('/addMovie', validate, tokenVerify, require('./addMovie'))

// Get movie
router.get('/getMovies', tokenVerify, require('./getMovies'))

// Upload movie poster
router.post('/uploadPoster', tokenVerify, require('./uploadMoviePic'))

// Latest Movies
router.get('/latestMovies', require('./freeLatestMovies'))

// Movies acc. to genre
router.post('/movieAccGenre', validate, tokenVerify, require('./movieAccGenre'))

// Search movies by movieName or actorName
router.post('/search', tokenVerify, require('./search'))

module.exports = router