const dbUserLogin = require('../../model/userLogin')

module.exports = async(req, res) => {
    try {

        let userData = await dbUserLogin.findOne({ $or: [{ email: req.body.email }, { phone: req.body.phone }] })

        if (userData || userData != null) {
            res.json({
                success: false,
                msg: "User already exists."
            })
        } else {
            let savedData = await new dbUserLogin({
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                userName: req.body.firstName + req.body.lastName,
                email: req.body.email,
                phone: req.body.phone,
                password: req.body.password
            }).save()

            if (!savedData) {
                res.json({
                    success: false,
                    msg: "Error while saving user data. Please try after some time."
                })
            } else {
                res.json({
                    success: true,
                    msg: "User successfully registered."
                })
            }
        }

    } catch (err) {
        console.log(`Exception caught in ${__filename} with error :- ${err}`)
        res.json({
            success: false,
            msg: err
        })
    }

}