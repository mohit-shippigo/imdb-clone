const dbMovie = require('../../model/movies')

module.exports = async(req, res) => {
    try {
        if (req.decoded.user == 'admin') {
            res.json({
                success: false,
                msg: "You are not allowed to add comment."
            })
        } else {
            let commenter = await dbMovie.findOne({ $and: [{ movieName: req.body.movieName }, { 'comments.commenter': req.decoded.userName }] })
            if (commenter) {
                res.json({
                    success: false,
                    msg: 'You have already commented on this movie.'
                })
            } else {

                let update = {
                    $push: { comments: { commenter: req.decoded.userName, content: req.body.content } }
                }

                let comment = await dbMovie.findOneAndUpdate({ movieName: req.body.movieName }, update)

                if (!comment) {
                    res.json({
                        success: false,
                        msg: "No such movie found in database."
                    })
                } else {
                    res.json({
                        success: true,
                        msg: comment
                    })
                }
            }
        }



    } catch (err) {
        console.log(`Exception caught in ${__filename} with error :- ${err}`)
        res.json({
            success: false,
            msg: err
        })
    }

}