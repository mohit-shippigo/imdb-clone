const registerUser = require('./registerUser')
const login = require('./login')
const logout = require('./logout')
const addComment = require('./addComment')
const movieRating = require('./movieRating')

module.exports = (req, res, next) => {
    // console.log('==-=-==-=->>>>', __filename,'original url:::',req.originalUrl)
    const url = req.originalUrl.split('/');
    // console.log('==-=-==-=->>>>', url)
    const finalUrl = url[url.length - 1]
    switch (finalUrl) {

        /**
         * Register User
         */
        case 'register':
            registerUser(req, res, next)
            break;


            /**
             * User Login
             */
        case 'login':
            login(req, res, next)
            break;


            /**
             * User logout
             */
        case 'logout':
            logout(req, res, next)
            break;


            /**
             * User Comment
             */
        case 'addComment':
            addComment(req, res, next)
            break;


            /**
             * User Movie Rating
             */
        case 'movieRating':
            movieRating(req, res, next)
            break;


        default:
            next();
    }

}