const regex = require('../../common/regex');

module.exports = (req, res, next) => {
    if (!req.body.movieName || !req.body.content) {
        res.json({
            success: false,
            msg: "Please enter movieName & comment."
        })
    } else {
        req.body.movieName = req.body.movieName.toLowerCase().trim()
        req.body.content = req.body.content.toLowerCase().trim()

        let contentCheck = regex.regexWordsWithSpace.test(req.body.content)

        if (!contentCheck) {
            res.json({
                success: false,
                msg: "Invalid movie or comment format."
            })
        } else {
            next();
        }
    }
}