const regex = require('../../common/regex');

module.exports = (req, res, next) => {

    if (!req.body.email || !req.body.password) {
        res.json({
            success: false,
            msg: "Please enter all details."
        })
    } else {
        req.body.email = req.body.email.toLowerCase().trim()
        req.body.password = req.body.password.trim()

        let emailCheck = regex.emailRegex.test(req.body.email)
        let passwordCheck = regex.passwordRegex.test(req.body.password)

        if (!emailCheck || !passwordCheck) {
            res.json({
                success: false,
                msg: "Invalid data format."
            })
        } else {
            next();
        }
    }
}