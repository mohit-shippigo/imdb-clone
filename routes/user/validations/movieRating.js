const regex = require('../../common/regex');

module.exports = (req, res, next) => {

    if (!req.body.rating || !req.body.movieName) {
        res.json({
            success: false,
            msg: "Please enter rating & movie name."
        })
    } else {
        req.body.movieName = req.body.movieName.toLowerCase().trim()
        req.body.rating = req.body.rating.trim()

        let ratingCheck = regex.regexNum.test(req.body.rating)

        if (!ratingCheck) {
            res.json({
                success: false,
                msg: "Invalid data format."
            })
        } else {
            next();
        }
    }
}