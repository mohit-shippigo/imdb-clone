const dbMovie = require('../../model/movies')

module.exports = async(req, res) => {
    try {
        if (req.decoded.user == 'admin') {
            res.json({
                success: false,
                msg: "You are not allowed to give movie rating."
            })
        } else {
            if (req.body.rating < 0 || req.body.rating > 5) {
                res.json({
                    success: false,
                    msg: "Minimun rating is 0 & Maximum rating is 5."
                })
            } else {

                let user = await dbMovie.findOne({ $and: [{ movieName: req.body.movieName }, { 'rating.by': req.decoded.userName }] })

                if (user) {
                    res.json({
                        success: false,
                        msg: 'You have already rated this movie.'
                    })
                } else {

                    let update = {
                        $push: { rating: { by: req.decoded.userName, rate: req.body.rating } }
                    }

                    let rate = await dbMovie.findOneAndUpdate({ movieName: req.body.movieName }, update)

                    if (!rate) {
                        res.json({
                            success: false,
                            msg: "No such movie found in database."
                        })
                    } else {
                        res.json({
                            success: true,
                            msg: rate
                        })
                    }
                }

            }
        }

    } catch (err) {
        console.log(`Exception caught in ${__filename} with error :- ${err}`)
        res.json({
            success: false,
            msg: err
        })
    }

}