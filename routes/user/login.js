const dbUserLogin = require("../../model/userLogin")
const jwt = require("jsonwebtoken")

exports.login = async(req, res) => {
    try {

        let loginData = await dbUserLogin.findOne({ email: req.body.email })

        if (!loginData || loginData == null)
            res.json({
                success: false,
                msg: "Please register First."
            })
        else {
            let isMatch = await loginData.comparePassword(req.body.password)

            if (!isMatch) {
                res.json({
                    success: false,
                    msg: "Incorrect Password."
                })
            } else {
                if (isMatch || loginData.password === req.body.password) {

                    let tokenData = {
                        userName: loginData.userName,
                        email: loginData.email,
                        phone: loginData.phone,
                        user: 'user'
                    }
                    let token = jwt.sign(tokenData, req.app.get('secretKey'))

                    let data = await dbUserLogin.findOneAndUpdate({ email: req.body.email }, { $push: { lastLogin: new Date() }, $set: { token: token } })

                    if (!data)
                        res.json({
                            success: false,
                            msg: "Something went wrong."
                        })
                    else {
                        res.json({
                            success: true,
                            msg: "Login Successfull",
                            token: token,
                        })
                    }


                }
            }
        }

    } catch (err) {
        console.log(`Exception caught in ${__filename} with error :- ${err}`)
        res.json({
            success: false,
            msg: err
        })
    }
}

exports.logout = async(req, res) => {
    try {
        let logout = await dbUserLogin.findOneAndUpdate({ email: req.body.email }, { $set: { token: null } })

        if (!logout) {
            res.json({
                success: false,
                msg: "No such email id found in database."
            })
        } else if (!logout.token) {
            res.json({
                success: false,
                msg: "Already logged out."
            })
        } else {
            res.json({
                success: true,
                msg: "Logout succcessfully"
            })
        }

    } catch (err) {
        console.log(`Exception caught in ${__filename} with error :- ${err}`)
        res.json({
            success: false,
            msg: err
        })
    }

}