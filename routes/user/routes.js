const express = require('express');
const router = express.Router();
const tokenVerify = require('../common/tokenVerify');
const validate = require('./validations/validate')


/************* Registered User **************/
router.post('/register', validate, require('./registerUser'))


/************* Login User **************/
router.post('/login', validate, require('./login').login)


/************* Logout User **************/
router.post('/logout', validate, tokenVerify, require('./login').logout)


/************* Comment on movie **************/
router.post('/addComment', validate, tokenVerify, require('./addComment'))


/************* Movie Rating **************/
router.post('/movieRating', validate, tokenVerify, require('./movieRating'))



module.exports = router