module.exports = function(grunt) {
    grunt.registerTask('speak', function() {
        console.log("I am speaking");
    });

    grunt.registerTask('learn', function() {
        console.log('I am learning');
    });

    grunt.registerTask('default', ['speak', 'learn'])
}