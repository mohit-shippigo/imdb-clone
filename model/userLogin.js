const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
    },
    lastName: {
        type: String,
        required: true,
    },
    userName: String,
    email: {
        type: String,
        required: true,
        unique: true
    },
    phone: {
        type: Number,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
    },
    token: String,
    lastLogin: [Date],
    lastLogout: [Date]
})


// we hased plain password b4 saving into database
userSchema.pre('save', async function(next) {
    try {
        let user = this;

        // Generating a salt
        let salt = await bcrypt.genSalt(SALT_WORK_FACTOR)

        // Hash the password using our new salt
        let hashed = await bcrypt.hash(user.password, salt)

        // Override the cleartext password with the hashed one
        user.password = hashed;
        next();
    } catch (err) {
        next(err)
    }


})

// we hased plain password b4 updating & saving into database
userSchema.pre('findOneAndUpdate', async function(next) {
    try {
        let user = this;

        if (!user._update.$set) {
            next()
        } else if (!user._update.$set.password) {
            next();
        } else {

            // Generating a salt
            let salt = await bcrypt.genSalt(SALT_WORK_FACTOR)

            // Hash the password using our new salt
            let hashed = await bcrypt.hash(user.password, salt)

            // Override the cleartext password with the hashed one
            user.password = hashed;
            next();
        }
    } catch (err) {
        next(err)
    }
})


// Now we compare hased password with normal password that user will enter while logging 
userSchema.methods.comparePassword = async function(newPassword) {
    try {
        return await bcrypt.compare(newPassword, this.password)
    } catch (err) {
        throw new Error(err)
    }
};

module.exports = mongoose.model("user", userSchema)