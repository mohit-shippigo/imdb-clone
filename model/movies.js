const mongoose = require('mongoose');

const movieSchema = new mongoose.Schema({
    movieName: {
        type: String,
        required: true,
        unique: true
    },
    director: String,
    cast: [String],
    imageName: [String],
    genre: String,
    description: String,
    releaseDate: Date,
    trailerLink: String,
    rating: [{
        by: String,
        rate: Number
    }],
    comments: [{
        commenter: String,
        content: String
    }]
})

module.exports = mongoose.model("movie", movieSchema)