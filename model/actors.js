const mongoose = require('mongoose');

const actorSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
    },
    lastName: {
        type: String,
        required: true,
        unique: true
    },
    userName: String,
    age: Number,
    imageName: [String],
    movies: [String],
})

module.exports = mongoose.model("actor", actorSchema)