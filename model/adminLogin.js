const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');


var adminLogin = new Schema({
    email: String,
    password: String,
    firstName: String,
    lastName: String,
    token: String
})


adminLogin.pre('save', async function(next) {
    try {
        let user = this;

        // Generating a salt
        let salt = await bcrypt.genSalt(10)

        // Hash the password using our new salt
        let hashed = await bcrypt.hash(user.password, salt)

        // Override the cleartext password with the hashed one
        user.password = hashed;
        next();
    } catch (err) {
        next(err)
    }
})


// Now we compare hased password with normal password that user will enter while logging 
adminLogin.methods.comparePassword = async function(newPassword) {
    try {
        return await bcrypt.compare(newPassword, this.password)
    } catch (err) {
        throw new Error(err)
    }
};

module.exports = mongoose.model('adminlogin', adminLogin)